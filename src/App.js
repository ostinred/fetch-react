import React, { Component } from 'react';

export default class App extends Component {
  state = {
    cite: ''
  };

  componentDidMount(){
    this.fetchCite();
  }

  fetchCite = () => {
    fetch('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
    .then(response => response.json())
    .then(data => this.setState({cite: data}))
    .catch(error => console.log('Request failed', error));
  }

  render() {
    return (
      <div className="App">
        <p>{this.state.cite}</p>
        <button onClick={this.fetchCite}>
          New cite
        </button>
      </div>
    );
  }
}
